import React, { Component } from "react";
import Home from "./Home";
import PersonList from "./PersonList";
import PersonUpdate from "./PersonUpdate";
import MoviesList from "./MoviesList";

class App extends Component {
  render() {
    var user = {
      name: "Aguero",
      hobbies: ["Football"]
    };
    return (
      <div>
        <h1>Hello World</h1>

        <Home name={"Dipesh"} age={23} user={user} />
        <PersonList />
        <PersonUpdate />
        <MoviesList />
      </div>
    );
  }
}

export default App;
