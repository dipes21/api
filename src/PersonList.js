import React from "react";

import axios from "axios";

export default class PersonList extends React.Component {
  state = {
    persons: [],
    emails: []
  };

  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/users`).then(res => {
      const persons = res.data;
      const emails = res.data;
      this.setState({ persons });
      this.setState({ emails });
    });
  }

  render() {
    return (
      <ul>
        {this.state.persons.map(person => (
          <li>{person.name}</li>
        ))}
        {this.state.emails.map(email => (
          <li>{email.email}</li>
        ))}
      </ul>
    );
  }
}
