import React from "react";

import axios from "axios";

export default class MoviesList extends React.Component {
  state = {
    movies: []
  };

  componentDidMount() {
    axios
      .get(`https://yts.am/api/v2/list_movies.json`, {
        params: {
          quality: "720p",
          minimum_rating: "7.0"
        }
      })
      .then(res => {
        const movies = res.data.data.movies;
        console.log("movies", movies);

        this.setState({ movies });
      });
  }

  render() {
    return (
      <table>
        <tr>
          <th>Title</th>
          <th>Rating</th>
          <th>Language</th>
        </tr>

        {this.state.movies.map(movie => (
          <React.Fragment>
            <tr>
              <td>{movie.title}</td>

              <td>{movie.rating}</td>
              <td>{movie.language}</td>
            </tr>
          </React.Fragment>
        ))}
      </table>
    );
  }
}
