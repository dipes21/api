import React, { Component } from "react";

class Home extends Component {
  render() {
    return (
      <div>
        <p>I'm a new component</p>
        <p>
          Your name is {this.props.name} , Your age is {this.props.age}
        </p>
        <p>User Object => Name: {this.props.user.name}</p>
      </div>
    );
  }
}

export default Home;
